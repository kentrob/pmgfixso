﻿using System;

namespace PmgFixSO
{
    public struct CrmResultMessage
    {
        public Guid Id;
        public CrmResult CrmResult;

        public CrmResultMessage(Guid id, CrmResult result)
        {
            Id = id;
            CrmResult = result;
        }
    }
}