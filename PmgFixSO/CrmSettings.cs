﻿namespace PmgFixSO
{
    public class CrmSettings
    {
        public int AuthenticationType { get; set; }
        public string OrganisationName { get; set; }
        public string ServiceAccountName { get; set; }
        public string ServiceAccountPassword { get; set; }
        public string Domain { get; set; }
        public string CrmUrl { get; set; }
    }
}