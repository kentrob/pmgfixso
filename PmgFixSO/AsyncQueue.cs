﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PmgFixSO
{
    public class AsyncQueue
    {
        private readonly RuntimeParameters parameters;
        private readonly object locker = new object();
        private readonly CrmClient crmClient;
        private readonly TaskCompletionSource<bool> taskCompletionSource = new TaskCompletionSource<bool>();
        private CancellationToken cancelToken;
        private ConcurrentQueue<string> idQueue = new ConcurrentQueue<string>();
        private int threadCount;
        private int crmErrorCount;
        private int processedCount;

        public AsyncQueue(RuntimeParameters parameters)
        {
            this.parameters = parameters;
            crmClient = new CrmClient();
        }

        public async Task<TaskCompletionSource<bool>> StartAsync(CancellationToken cancellationToken,
            IEnumerable<string> ids)
        {
            cancelToken = cancellationToken;

            foreach (var id in ids)
            {
                idQueue.Enqueue(id);
            }
            threadCount = 0;

            // Prime our thread pump with max threads.
            for (var i = 0; i < parameters.MaxThreads; i++)
            {
                await StartCrmRequest();
            }

            return taskCompletionSource;
        }

        private async Task StartCrmRequest()
        {
            if (taskCompletionSource.Task.IsCompleted)
            {
                return;
            }

            if (cancelToken.IsCancellationRequested)
            {
                Program.TellUser("Crm client cancelling...");
                ClearQueue();
                return;
            }

            var count = GetThreadCount();

            if (count >= parameters.MaxThreads)
            {
                return;
            }

            string id;
            if (!idQueue.TryDequeue(out id)) return;

            IncrementThreadCount();
            var crmMessage = await crmClient.CompleteActivityAsync(new Guid(id), parameters.CallIntervalMsecs);
            ProcessResult(crmMessage);

            processedCount += 1;
            if (parameters.TellUserAfterNCalls > 0 && processedCount%parameters.TellUserAfterNCalls == 0)
            {
                ShowProgress(processedCount);
            }
        }

        private async void ProcessResult(CrmResultMessage response)
        {
            if (response.CrmResult == CrmResult.Failed && ++crmErrorCount == parameters.CrmErrorsBeforeQuitting)
            {
                Program.TellUser(
                    "Quitting because CRM error count is equal to {0}. Already queued web service calls will have to run to completion.",
                    crmErrorCount);
                ClearQueue();
            }

            var count = DecrementThreadCount();

            if (idQueue.Count == 0 && count == 0)
            {
                taskCompletionSource.SetResult(true);
            }
            else
            {
                await StartCrmRequest();
            }
        }

        private int GetThreadCount()
        {
            lock (locker)
            {
                return threadCount;
            }
        }

        private void IncrementThreadCount()
        {
            lock (locker)
            {
                threadCount = threadCount + 1;
            }
        }

        private int DecrementThreadCount()
        {
            lock (locker)
            {
                threadCount = threadCount - 1;
                return threadCount;
            }
        }

        private void ClearQueue()
        {
            idQueue = new ConcurrentQueue<string>();
        }

        private static void ShowProgress(int processedCount)
        {
            Program.TellUser("{0} activities processed.", processedCount);
        }
    }
}