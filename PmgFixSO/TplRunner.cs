﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace PmgFixSO
{
    public static class TplRunner
    {
        private static readonly CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

        public static void StartQueue(RuntimeParameters parameters, IEnumerable<string> idList)
        {
            Console.CancelKeyPress += (s, args) =>
            {
                CancelCrmClient();
                args.Cancel = true;
            };

            var start = DateTime.Now;
            Program.TellUser("Start: " + start);

            var taskCompletionSource = new TplQueue(parameters)
                .Start(CancellationTokenSource.Token, idList);

            while (!taskCompletionSource.Task.IsCompleted)
            {
                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key != ConsoleKey.Q) continue;
                    Console.WriteLine("When all threads are complete, press any key to continue.");
                    CancelCrmClient();
                }
            }

            var end = DateTime.Now;
            Program.TellUser("End: {0}. Elapsed = {1} secs.", end, (end - start).TotalSeconds);
        }

        private static void CancelCrmClient()
        {
            CancellationTokenSource.Cancel();
            Console.WriteLine("Cancelling Crm client. Web service calls in operation will have to run to completion.");
        }
    }
}