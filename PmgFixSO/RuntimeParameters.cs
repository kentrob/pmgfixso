﻿namespace PmgFixSO
{
    public struct RuntimeParameters
    {
        public int MaxThreads;
        public int CrmErrorsBeforeQuitting;
        public int TellUserAfterNCalls;
        public int CallIntervalMsecs;
        public bool UseAsyncAwait;

        public RuntimeParameters(int maxThreads, int crmErrorsBeforeQuitting, int callIntervalMsecs, int tellUserAfterNCalls, bool useAsyncAwait)
        {
            MaxThreads = maxThreads;
            CrmErrorsBeforeQuitting = crmErrorsBeforeQuitting;
            TellUserAfterNCalls = tellUserAfterNCalls;
            UseAsyncAwait = useAsyncAwait;
            CallIntervalMsecs = callIntervalMsecs;
        }

        public override string ToString()
        {
            return string.Format(
                "MaxThreads = {0}; CrmErrorsBeforeQuitting = {1}; CallIntervalMsecs = {2}; TellUserAfterNCalls = {3}; UseAsyncAwait = {4}",
                MaxThreads,
                CrmErrorsBeforeQuitting,
                CallIntervalMsecs,
                TellUserAfterNCalls,
                UseAsyncAwait);
        }
    }
}