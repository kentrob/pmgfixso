﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PmgFixSO
{
    public class TplQueue
    {
        private readonly RuntimeParameters parameters;
        private readonly object locker = new object();
        private ConcurrentQueue<string> idQueue = new ConcurrentQueue<string>();
        private readonly CrmClient crmClient;
        private readonly TaskCompletionSource<bool> taskCompletionSource = new TaskCompletionSource<bool>();
        private int threadCount;
        private int crmErrorCount;
        private int processedCount;
        private CancellationToken cancelToken;

        public TplQueue(RuntimeParameters parameters)
        {
            this.parameters = parameters;
            crmClient = new CrmClient();
        }

        public TaskCompletionSource<bool> Start(CancellationToken cancellationToken, IEnumerable<string> ids)
        {
            cancelToken = cancellationToken;

            foreach (var id in ids)
            {
                idQueue.Enqueue(id);
            }

            threadCount = 0;

            // Prime our thread pump with max threads.
            for (var i = 0; i < parameters.MaxThreads; i++)
            {
                Task.Run((Action) StartCrmRequest, cancellationToken);
            }

            return taskCompletionSource;
        }

        private void StartCrmRequest()
        {
            if (taskCompletionSource.Task.IsCompleted)
            {
                return;
            }

            if (cancelToken.IsCancellationRequested)
            {
                Program.TellUser("Crm client cancelling...");
                ClearQueue();
                return;
            }

            var count = GetThreadCount();

            if (count >= parameters.MaxThreads)
            {
                return;
            }

            string id;
            if (!idQueue.TryDequeue(out id)) return;

            IncrementThreadCount();
            crmClient.CompleteActivityAsync(new Guid(id), parameters.CallIntervalMsecs).ContinueWith(ProcessResult);

            processedCount += 1;
            if (parameters.TellUserAfterNCalls > 0 && processedCount%parameters.TellUserAfterNCalls == 0)
            {
                ShowProgress(processedCount);
            }
        }

        private void ProcessResult(Task<CrmResultMessage> response)
        {
            if (response.Result.CrmResult == CrmResult.Failed && ++crmErrorCount == parameters.CrmErrorsBeforeQuitting)
            {
                Program.TellUser(
                    "Quitting because CRM error count is equal to {0}. Already queued web service calls will have to run to completion.",
                    crmErrorCount);
                ClearQueue();
            }

            var count = DecrementThreadCount();

            if (idQueue.Count == 0 && count == 0)
            {
                taskCompletionSource.SetResult(true);
            }
            else
            {
                StartCrmRequest();
            }
        }

        private int GetThreadCount()
        {
            lock (locker)
            {
                return threadCount;
            }
        }

        private void IncrementThreadCount()
        {
            lock (locker)
            {
                threadCount = threadCount + 1;
            }
        }

        private int DecrementThreadCount()
        {
            lock (locker)
            {
                threadCount = threadCount - 1;
                return threadCount;
            }
        }

        private void ClearQueue()
        {
            idQueue = new ConcurrentQueue<string>();
        }

        private static void ShowProgress(int processedCount)
        {
            Program.TellUser("{0} activities processed.", processedCount);
        }
    }
}