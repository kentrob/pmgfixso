﻿namespace PmgFixSO
{
    public enum CrmResult
    {
        Succeeded,
        Failed,
        NotRequired,
        NotFound
    }
}