﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace PmgFixSO
{
    internal class Program
    {
        private const string FilePath = "PmgFixSO.guids.txt";

        private static void Main()
        {
            try
            {
                var settings = ReadConfigSettings();
                var idList = ReadIdsFromInputFile();
                var threadingModel = settings.UseAsyncAwait ? "AsyncAwait" : "TPL";

                if (idList.Count == 0)
                {
                    TellUser("Id list was empty or contained only blank strings. Nothing to process.");
                    return;
                }

                TellUser("{0} lines found in input file.", idList.Count);
                TellUser(
                    "Starting CRM queue (type {0}). Press 'q' or Ctrl+C to quit (there may be a short delay until the console thread is active):\n",
                    threadingModel);

                if (settings.UseAsyncAwait)
                    AsyncRunner.StartQueue(settings, idList);
                else
                    TplRunner.StartQueue(settings, idList);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }
        }

        public static void TellUser(string format, params object[] args)
        {
            TellUser(format, true, args);
        }

        public static void TellUser(string format, bool withNewline, params object[] args)
        {
            if (withNewline)
                Console.WriteLine(format, args);
            else
                Console.Write(format, args);
        }

        private static List<string> ReadIdsFromInputFile()
        {
            var result = new List<string>();
            using (var reader = OpenInputFile())
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!String.IsNullOrWhiteSpace(line))
                        result.Add(line);
                }
            }
            return result;
        }

        private static StreamReader OpenInputFile()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var stream = assembly.GetManifestResourceStream(FilePath);
            if (stream == null)
            {
                throw new ApplicationException("Failed to read embedded resource: " + FilePath);
            }
            return new StreamReader(stream);
        }

        private static RuntimeParameters ReadConfigSettings()
        {
            int tellUserAfterNCalls;
            int crmErrorsBeforeQuitting;
            int maxThreads;
            int callIntervalMsecs;
            bool useAsyncAwait;

            if (!Int32.TryParse(ConfigurationManager.AppSettings["CallIntervalMsecs"], out callIntervalMsecs))
            {
                callIntervalMsecs = 0;
            }

            if (!Int32.TryParse(ConfigurationManager.AppSettings["MaxThreads"], out maxThreads))
            {
                maxThreads = 10;
            }

            if (!Int32.TryParse(ConfigurationManager.AppSettings["CrmErrorsBeforeQuitting"],
                out crmErrorsBeforeQuitting))
            {
                crmErrorsBeforeQuitting = 5;
            }

            if (!Int32.TryParse(ConfigurationManager.AppSettings["TellUserAfterNCalls"],
                out tellUserAfterNCalls))
            {
                tellUserAfterNCalls = 5;
            }

            if (!bool.TryParse(ConfigurationManager.AppSettings["UseAsyncAwait"],
                out useAsyncAwait))
            {
                useAsyncAwait = false;
            }

            return new RuntimeParameters(maxThreads, crmErrorsBeforeQuitting, callIntervalMsecs, tellUserAfterNCalls,
                useAsyncAwait);
        }
    }
}