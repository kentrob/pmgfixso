﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace PmgFixSO
{
    public class CrmClient
    {
        public Task<CrmResultMessage> CompleteActivityAsync(Guid activityId, int callIntervalMsecs)
        {
            // Here we would normally call a CRM web service.
            return Task.Run(() =>
            {
                try
                {
                    if (callIntervalMsecs > 0)
                    {
                        Thread.Sleep(callIntervalMsecs);
                    }
                    throw new ApplicationException("Crm web service not available at the moment.");
                }
                catch
                {
                    return new CrmResultMessage(activityId, CrmResult.Failed);
                }
            });
        }
    }
}